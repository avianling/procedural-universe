﻿using UnityEngine;
using System.Collections;

[System.Serializable]
// Defines an area enclosed between two points.
public class intBounds {
	public int3 min;
	public int3 max;

	public int3 sampleTrilinear( float x, float y, float z ) {
		int3 point = min;
		point.x = min.x + (int)(x * (float)(max.x-min.x));
		point.y = min.y + (int)(y * (float)(max.y-min.y));
		point.z = min.z + (int)(z * (float)(max.z-min.z));

		return point;
	}

	public int3 size {
		get {
			int3 _size = new int3();
			_size.x = max.x - min.x;
			_size.y = max.y - min.y;
			_size.z = max.z - min.z;
			return _size;
		}
	}

	public Vector3 center {
		get {
			return new Vector3(
				0.5f * (float)( min.x + max.x ),
				0.5f * (float)( min.y + max.y ),
				0.5f * (float)( min.z + max.z )
				);
		}
	}

	public bool Contains( int3 point ) {
		return point.x >= min.x && point.x <= max.x && point.y >= min.y && point.y <= max.y && point.z >= min.z && point.z <= max.z;
	}

	public static intBounds operator +( intBounds me, int3 offset ) {
		me.max = me.max + offset;
		me.min = me.min + offset;
		return me;
	}

	public intBounds Clone() {
		intBounds clone = new intBounds ();
		clone.min = new int3 (min.x, min.y, min.z);
		clone.max = new int3 (max.x, max.y, max.z);
		return clone;
	}

}
