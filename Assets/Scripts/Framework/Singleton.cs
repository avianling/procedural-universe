﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : Singleton<T> {

	private static T _instance;
	
	public static T getSingleton() {
		if ( _instance == null ) {
			GameObject obj = new GameObject();
			obj.name = "Singleton";
			_instance = obj.AddComponent<T>();
		}
		return _instance;
	}
	
	void Awake() {
		_instance = (T)this;
	}

}
