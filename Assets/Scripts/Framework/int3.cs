﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct int3 {
	public int x,y,z;

	public int3( int x, int y, int z ) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public override int GetHashCode ()
	{
		// simplistic hash - we will likely never be working on an area over 100 units in size at once, so this should be perfectly save...
		return (x) + (y * 500) + (z * 250000);
	}

	public override string ToString ()
	{
		return string.Format ("[int3:{0},{1},{2}]", x, y, z);
	}

	public float magnitude {
		get {
			return Mathf.Sqrt( x*x + y*y + z*z );
		}
	}

	public Vector3 vector3 {
		get {
			return new Vector3( x, y, z );
		}
	}

	public uint manhatten {
		get {
			return System.Convert.ToUInt32( System.Math.Abs(x) ) + System.Convert.ToUInt32( System.Math.Abs(y) ) + System.Convert.ToUInt32( System.Math.Abs(z) );
		}
	}

	public static int3 operator *(int3 me, float scalar) {
		me.x = (int)(scalar * me.x);
		me.y = (int)(scalar * me.y);
		me.z = (int)(scalar * me.z);
		return me;
	}

	public static int3 operator +(int3 me, int3 other ) {
		me.x += other.x;
		me.y += other.y;
		me.z += other.z;
		return me;
	}

	public static int3 operator -(int3 me, int3 other ) {
		me.x -= other.x;
		me.y -= other.y;
		me.z -= other.z;
		return me;
	}

	public static bool operator !=(int3 me, int3 other ) {
		return me.x != other.x || me.y != other.y || me.z != other.z;
	}

	public static bool operator ==(int3 me, int3 other ) {
		return me.x == other.x && me.y == other.y && me.z == other.z;
	}

	public override bool Equals (object obj)
	{
		if (obj is int3) {
			return ((int3)obj) == this;
		} else {
			return base.Equals(obj);
		}
	}
}
