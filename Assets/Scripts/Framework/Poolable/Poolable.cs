﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Poolable.
/// A poolable object with state data of type T.
/// </summary>
public abstract class Poolable<U, T> : MonoBehaviour where U : Poolable<U, T> {

	// The list of pooled objects.
	private static List<Poolable<U, T>> _pool = new List<Poolable<U, T>>();

	// how to create this object?
	public static System.Func< T, U > factory;

	public static U create( T state ) {
		U obj = null;
		if (_pool.Count > 0) {
			obj = (U)_pool [0];
			_pool.RemoveAt (0);
		} else {
			obj = factory(state);
		}
		obj.state = state;
		obj.gameObject.SetActive (true);
		obj.init (state);
		return obj;
	}

	public T state;

	public abstract void init (T state);

	protected virtual void onRelease() {
		
	}
	
	// Return an object to the pool.
	public void release() {
		onRelease ();
		_pool.Add (this);
		state = default(T);
		gameObject.SetActive (false);
	}

}

public abstract class Poolable<U, T1, T2> : MonoBehaviour where U : Poolable<U, T1, T2> {
	
	// The list of pooled objects.
	private static List<Poolable<U, T1, T2>> _pool = new List<Poolable<U, T1, T2>>();
	
	// how to create this object?
	public static System.Func< T1, T2, U > factory;
	
	public static U create( T1 param1, T2 param2 ) {
		U obj = null;
		if (_pool.Count > 0) {
			obj = (U)_pool [0];
			_pool.RemoveAt (0);
		} else {
			obj = factory( param1, param2 );
		}
		obj.param1 = param1;
		obj.param2 = param2;
		obj.init (param1, param2);
		obj.gameObject.SetActive (true);
		return obj;
	}
	
	public T1 param1;
	public T2 param2;
	
	protected abstract void init (T1 param1, T2 param2);

	protected virtual void onRelease() {

	}

	// Return an object to the pool.
	public void release() {
		onRelease ();
		_pool.Add (this);
		param1 = default(T1);
		param2 = default(T2);
		gameObject.SetActive (false);
	}
	
}
