﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Alex.Procedural.Universe {
	
	public class UniverseView : ProceduralView {

		public int lod0Range = 3;

		// The view will recenter and rebuild itself around this pawn as the pawn enters new areas.
		public GameObject pawn;

		public Gradient backgroundColours;
		public float backgroundNoiseFrequency;

		void Awake() {
			// Define the constructors for these stars.
			UniverseStar_0.factory = ( (star, view) => {
				return fabricator.starFabricator.fabricateStar_Universe( star, 0 ).GetComponent<UniverseStar_0>();
			});
			UniverseStar_1.factory = ((star, view) => {
				return fabricator.starFabricator.fabricateStar_Universe( star, 1 ).GetComponent<UniverseStar_1>();
			});

			lastArea = new intBounds ();
			lastArea.max = new int3 (0, 0, 0);
			lastArea.min = new int3 (0, 0, 0);
		}

		private int calculateRequiredLOD( Star star, Vector3 centerSector ) {
			if ((star.sector.position.vector3 - centerSector).sqrMagnitude > lod0Range*lod0Range) {
				return 1;
			}
			return 0;
		}

		private List<Star> neededStars = new List<Star>();
		private Dictionary<int3, UniverseStar_Base> currentStars = new Dictionary<int3, UniverseStar_Base>();
		private Dictionary<Star, int> loadedLODs = new Dictionary<Star, int>();

		// store the last area we rendererd, to avoid unneeded recomputation
		private intBounds lastArea;

		private UniverseStar_Base createStar( Star star, int lod ) {
			if (lod == 0) {
				return UniverseStar_0.create (star, this );
			} else {
				return UniverseStar_1.create (star, this );
			}
		}
			
		protected override IEnumerator rebuildView() {
			// Build a model of the universe.
			// Iterate through the sectors of the universe.
			// Maybe encode this into the universe itself?

			if (area.min.x < -model.universe.halfSize) {
				area.min.x = -model.universe.halfSize;
			}
			if (area.min.y < -model.universe.halfSize) {
				area.min.y = -model.universe.halfSize;
			}
			if (area.min.z < -model.universe.halfSize) {
				area.min.z = -model.universe.halfSize;
			}

			if (area.min.x > model.universe.halfSize - size.x) {
				area.min.x = model.universe.halfSize - size.x;
			}
			if (area.min.y > model.universe.halfSize - size.y) {
				area.min.y = model.universe.halfSize - size.y;
			}
			if (area.min.z > model.universe.halfSize - size.z) {
				area.min.z = model.universe.halfSize - size.z;
			}

			if (area.max.x > model.universe.halfSize) {
				area.max.x = model.universe.halfSize;
			}
			if (area.max.y > model.universe.halfSize) {
				area.max.y = model.universe.halfSize;
			}
			if (area.max.z > model.universe.halfSize) {
				area.max.z = model.universe.halfSize;
			}

			if (area.max.x < -model.universe.halfSize + size.x) {
				area.max.x = -model.universe.halfSize + size.x;
			}
			if (area.max.y < -model.universe.halfSize + size.y) {
				area.max.y = -model.universe.halfSize + size.y;
			}
			if (area.max.z < -model.universe.halfSize + size.z) {
				area.max.z = -model.universe.halfSize + size.z;
			}
	
			//yield return 0;
			Vector3 _areaCenter = area.center;

			/*_neededStarSet.Clear ();
			neededStars.Clear ();
			requiredLODs.Clear ();*/

			// For every star which we have loaded which is not in the currently requested area, mark it as unneeded.
			for (int i=0; i<neededStars.Count; i++) {
				if ( !area.Contains(neededStars[i].sector.position ) ) {
					// this star is not needed.
					loadedLODs.Remove( neededStars[i] );
					currentStars[ neededStars[i].sector.position ].release();
					currentStars.Remove( neededStars[i].sector.position );
					neededStars.RemoveAt(i);
					i--;
				} else {
					// Sort out LOD stuff here?
					int requiredLOD = calculateRequiredLOD( neededStars[i], _areaCenter );
					if ( requiredLOD != loadedLODs[ neededStars[i] ] ) {
						// Unload this star and create a new one with the correct LOD.
						currentStars[ neededStars[i].sector.position ].release();
						currentStars[ neededStars[i].sector.position ] = createStar( neededStars[i], requiredLOD );
						loadedLODs[ neededStars[i] ] = requiredLOD;
					}
					// otherwise, this star is totally fine.
					currentStars[neededStars[i].sector.position].PositionStar();
				}
			}

			// Instead of clearing everything, only clear it if it's not in the needed list?

			for ( int x = area.min.x; x < area.max.x; x++ ) {
				for ( int y = area.min.y; y < area.max.y; y++ ) {
					for ( int z = area.min.z; z < area.max.z; z++ ) {
						// If we haven't calculated this sector before, do so now.
						if ( !lastArea.Contains( new int3(x,y,z) ) ) {
							Sector sector = model.generator.universeGenerator.generateSector( new int3( x,y,z ), model.universe );
							Star[] stars = model.generator.starGenerator.GenerateStarsForSector( sector, model.universe );
							for ( int i=0; i < stars.Length; i++ ) {
								// smart replace
								// Compute this star and add it to the list.
								int requiredLOD = calculateRequiredLOD( stars[i], _areaCenter );
								UniverseStar_Base newStar = createStar ( stars[i], requiredLOD );
								neededStars.Add( stars[i] );
								loadedLODs.Add( stars[i], requiredLOD );
								currentStars.Add( stars[i].sector.position, newStar );

								// max 1 star atm.
								break;
							}
						}
					}
				}
			}

			lastArea = area.Clone();

			yield break;
			
		}

		protected override void clearView ()
		{
			foreach (KeyValuePair< int3, UniverseStar_Base > currentStar in currentStars) {
				currentStar.Value.release();
			}
			neededStars.Clear ();
			loadedLODs.Clear ();
			currentStars.Clear ();
		}

		private int3 _oldPawnSector;
		private int3 _newPawnSector;

		protected override void Update ()
		{
			base.Update ();

			// If the pawn is in a new sector,
			// calculate the offset of the player to the center of this new sector
			// recenter the area on the new sector
			// move the player to be at that offset as their absolute position
			// rebuild the view.

			// Calculate the current sector of the pawn.
			Vector3 _pawnPos = pawn.transform.position / gameSizeOfSector;
			_newPawnSector = new int3 (
				Mathf.RoundToInt (_pawnPos.x),
				Mathf.RoundToInt (_pawnPos.y),
				Mathf.RoundToInt (_pawnPos.z)
			);

			if ( _oldPawnSector != _newPawnSector ) {
				// Calculate the position of the new sector in game coordinates.
				Vector3 newSectorCenter = _newPawnSector.vector3 * gameSizeOfSector;
				Vector3 offset = pawn.transform.position - newSectorCenter;
				// Now that we have the offset of the players position, we can build the new world.
				area = area + ( _newPawnSector - _oldPawnSector );
				_rebuildView();
				pawn.transform.position = offset;
			}

			//Camera.main.backgroundColor = backgroundColours.Evaluate(SimplexNoise.Noise.Generate( area.center + pawn.transform.position, backgroundNoiseFrequency ) * 0.5f + 0.5f);
		}

	}

}