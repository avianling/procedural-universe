﻿using UnityEngine;
using System.Collections;

namespace Alex.Procedural.Universe {

	public abstract class ProceduralView : MonoBehaviour {

		// The systems needed to fabricate things for this view.
		public FabricationManager fabricator;

		// The model this view is based on.
		public ProceduralModel model;

		// general view properties relating to scale.
		public Vector3 gamePositionCenter;
		public float gameSizeOfSector = 1f;
		public intBounds area;
		public int3 size {
			get {
				return area.size;
			}
		}

		public float gameSizeOfAU {
			get {
				return gameSizeOfSector / model.universe.SectorSizeInAU;
			}
		}

		protected virtual void Update() {
			if ( Input.GetKeyDown (KeyCode.Backspace) ) {
				clearView ();
				_rebuildView();
			}
			
			if ( Time.deltaTime > 5f ) {
				Debug.Log ("Safety Kill Activated by excessive frame lag of " + Time.deltaTime );
				Debug.Break();
			}
		}
		
		void Start() {
			_rebuildView();
		}
		
		protected void _rebuildView() {
			//StopAllCoroutines();
			//clearView ();
			StartCoroutine(rebuildView());
		}

		protected abstract void clearView();

		protected abstract IEnumerator rebuildView();

	}

}