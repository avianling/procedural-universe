﻿using UnityEngine;
using System.Collections;

namespace Alex.Procedural.Universe {

	public class VolumetricUniverseView : ProceduralView {

		public int3 resolution;

		public bool wholeUniverse;

		private int getVolumeIndex( int3 position ) {
			return position.x + (resolution.x * position.y) + (resolution.x * resolution.y * position.z);
		}

		private Texture3D generateVolume(ProceduralModel model, intBounds bounds) {
			Texture3D volume = new Texture3D (resolution.x, resolution.y, resolution.z, TextureFormat.ARGB32, false);
			volume.wrapMode = TextureWrapMode.Clamp;

			float invX = 1f / resolution.x;
			float invY = 1f / resolution.y;
			float invZ = 1f / resolution.z;
			Sector sector;

			Color[] colours = new Color[resolution.x * resolution.y * resolution.z];

			for (int x = 0; x < resolution.x; x++) {
				for (int y = 0; y < resolution.y; y++) {
					for (int z = 0; z < resolution.z; z++) {
						sector = model.generator.universeGenerator.generateSector( bounds.sampleTrilinear( x*invX, y*invY, z*invZ ), model.universe );
						int index = getVolumeIndex( new int3( x, y, z) );
						//colours[ index ] = new Color(x * invX, y * invY, z * invZ, 1);
						//colours[index] = new Color( Random.Range(0f,1f),Random.Range(0f,1f),Random.Range(0f,1f),1);
						colours[ index ] = new Color(sector.probability, sector.probability, sector.probability, 1);
					}
				}
			}

			volume.SetPixels (colours);
			volume.Apply ();
			return volume;
		}

		protected override void clearView ()
		{
			// do nothing
		}

		protected override IEnumerator rebuildView ()
		{
			if (wholeUniverse) {
				area = new intBounds();
				area.min = new int3( -model.universe.halfSize,-model.universe.halfSize,-model.universe.halfSize );
				area.max = new int3(  model.universe.halfSize, model.universe.halfSize, model.universe.halfSize );
			}

			Texture3D volume = generateVolume(model, area);
			GetComponent<Renderer> ().sharedMaterial.SetTexture ("_Volume", volume);
			yield return 0;
		}
	
	}

}