﻿using UnityEngine;
using System.Collections;

namespace Alex.Procedural.Universe {
	
	/// <summary>
	/// The controller from which everything starts.
	/// Knows about the generators and fabricators and can be easilly accessed by the ProceduralView, which renders the created system.
	/// </summary>
	public class ProceduralModel : MonoBehaviour {
	
		public Universe universe;
		public GenerationManager generator;
		
	}

}