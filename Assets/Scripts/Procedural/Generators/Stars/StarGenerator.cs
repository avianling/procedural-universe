﻿using UnityEngine;
using System.Collections;

namespace Alex.Procedural.Universe {
	
	/// <summary>
	/// Star generator.
	/// A function for generating stars within a given universe.
	/// </summary>
	public abstract class StarGenerator : MonoBehaviour {
	
		public abstract Star[] GenerateStarsForSector( Sector sector, Universe universe );
		
	}

}