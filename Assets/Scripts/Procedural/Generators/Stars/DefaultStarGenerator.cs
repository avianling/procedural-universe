using UnityEngine;
using System.Collections;
using Alex.Procedural;

namespace Alex.Procedural.Universe {

	/// <summary>
	/// Spiral galaxy generator.
	/// Generates stars according to a distribution forming a spiral armed disc.
	/// Unless otherwise stated, units here are in UU ( Universe Units )
	/// </summary>
	public class DefaultStarGenerator : StarGenerator {
	
		public float starFrequency;
		public float colorNoiseFrequency = 2f;
		public float sizeNoiseFrequency = 2f;
		public Gradient color;

		public AnimationCurve sizeDistribution;
			
		public override Star[] GenerateStarsForSector( Sector sector, Universe universe ) {
			
			//float roll = Random.Range( 0f, float.MaxValue );
			/*float roll = SimplexNoise.Noise.Generate (sector.position.vector3, noiseFrequency1) * RegularnessControl;
			roll += SimplexNoise.Noise.Generate (sector.position.vector3, noiseFrequency2) * ( 1f - RegularnessControl);
			roll = roll * 0.5f + 0.5f;
			roll *= float.MaxValue;*/

			float roll = (1f - starFrequency);

			if ( sector.probability > roll ) {
				Star newStar = new Star();
				newStar.age = 1f;
				//newStar.age = Random.Range(0.1f, 1f);
				//newStar.size = sizeDistribution.Evaluate(  );
				newStar.size = sizeDistribution.Evaluate( Noise.FakeValue3D(sector.position.vector3, 3f * sizeNoiseFrequency ) );
				newStar.color = color.Evaluate(SimplexNoise.Noise.Generate( sector.position.vector3, colorNoiseFrequency ) * 0.5f + 0.5f);
				float value1 = Noise.Value1D( sector.position.x, colorNoiseFrequency );
				newStar.position = SimplexNoise.Noise.GenerateVector3( sector.position.vector3 );//new Vector3( Noise.Value1D( sector.position.x + sector.position.y, noiseFrequency )*2f - 1f, Noise.Value1D( sector.position.y + sector.position.z, noiseFrequency )*2f - 1f, Noise.Value1D( sector.position.z + sector.position.x, noiseFrequency )*2f - 1f );
				newStar.sector = sector;
				return new Star[]{ newStar };
			} else {
				return new Star[0];
			}
			
		}
		
	}

}