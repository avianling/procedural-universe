﻿using UnityEngine;
using System.Collections;

namespace Alex.Procedural.Universe {

	[System.Serializable]
	public class Star : ProceduralItem {
	
		public float age;

		public Color color;

		// The size of this star is in solar radii/
		// 1 solar radii = roughly 700000 km
		// is 109 times the radius of earth.
		// I'm not sure what fraction of the size of a universal sector this is, since that size remains undefined, but we'll go with something small.
		public float size;

		// Position of this star within the sector.
		// Allowable range: -1 to 1
		public Vector3 position;

		// The sector containing this star.
		public Sector sector;

		public override int GetHashCode ()
		{
			return sector.position.GetHashCode();
		}

		public override bool Equals (object obj)
		{
			if (obj is Star) {
				Star other = obj as Star;
				return other.sector.position == sector.position;
			} else {
				return base.Equals(obj);
			}
		}
		
		
	}
}