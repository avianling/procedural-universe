﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Alex.Procedural.Universe {
	
	/// <summary>
	/// Generation manager.
	/// Allows the player to create objects of particular forms.
	/// </summary>
	public class GenerationManager : MonoBehaviour {
	
		public UniverseGenerator universeGenerator;

		public StarGenerator starGenerator;
	
		// Old code for a more flexible but actually less useful system.
		// Switching to using just hardcoded methods.
		// Not quite as reusable for other projects, but I'll think about that later
		/*public delegate ProceduralItem GeneratorFunction( params object[] inputs );
		
		private Dictionary< System.Type, GeneratorFunction > _generatorFunctions = new Dictionary<System.Type, GeneratorFunction>();
	
		public void RegisterGenerationFunction<Type>( GeneratorFunction generatorFunction ) where Type : ProceduralItem {
			
		}
		
		public Type GenerateObject<Type>( params object[] inputs ) where Type : ProceduralItem {
			return null;
		}*/
		
		
		
	}
	
}