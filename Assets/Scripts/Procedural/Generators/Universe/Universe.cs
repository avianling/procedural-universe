﻿using UnityEngine;
using System.Collections;

namespace Alex.Procedural.Universe {
	
	/// <summary>
	/// Universe.
	/// A particular universe.
	/// A universe is really a generator function for stars.
	/// and stars are really generator functions for planets
	/// and planets are really generator functions for terrain & creatures etc.
	/// </summary>
	[System.Serializable]
	public class Universe : ProceduralItem {
	
		// The seed of this universe.
		public long seed;
		
		// The length of each axis of the universe.
		public int size;

		[Header("How wide is a sector in AU? A rough estimate of the size of a sector in the milky way is about 4million AU.")]
		public float SectorSizeInAU = 1000;
		[Header("How large is a typical star in AU in this universe? Sol is about 0.0046 au")]
		public float StarSizeInAU = 0.0046f;

		public int halfSize {
			get {
				return Mathf.RoundToInt(0.5f * size);
			}
		}
		
	}

}