﻿using UnityEngine;
using System.Collections;

namespace Alex.Procedural.Universe {

	public abstract class UniverseGenerator : MonoBehaviour {

		public abstract Sector generateSector( int3 position, Universe universe );
	}

}