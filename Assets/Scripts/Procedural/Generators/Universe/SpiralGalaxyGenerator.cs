﻿using UnityEngine;
using System.Collections;
using System;

namespace Alex.Procedural.Universe {

	public class SpiralGalaxyGenerator : UniverseGenerator {

		public AnimationCurve polarHorizontalDistribution;
		public AnimationCurve verticalDistribution;

		public const float sqrt2 = 1.414214f;

		[Range(0f,1f)]
		public float RegularnessControl;
		public float noiseFrequency1 = 0.00001f;
		public float noiseFrequency2 = 0.00001f;
		public int noiseOctaves = 1;

		public override Sector generateSector (int3 position, Universe universe)
		{
			// Calculate normalized distance from the center on horizontal and vertical axes.
			
			int3 tempVector = position;
			double normalizedVerticalDistance = Math.Abs(position.y / (double)universe.halfSize) * 2.0;
			tempVector.y = 0;
			double normalizedHorizontalDistance = (1.0 - Math.Abs(tempVector.magnitude / (universe.halfSize)));
			// Normalize the vertical distance again between 0 and the height allowed by the curve of the polarHorizontalDistribution.
			/*bool allowedHeight = normalizedVerticalDistance < polarHorizontalDistribution.Evaluate( normalizedHorizontalDistance );
		
			if ( !allowedHeight ) {
				return new Star[0];
			}*/
			
			// Scale the vertical distribution to fit between 0 and the maximum allowed height.
			normalizedVerticalDistance = (normalizedVerticalDistance) / verticalDistribution.Evaluate( (float)normalizedHorizontalDistance);
			
			double probability = polarHorizontalDistribution.Evaluate( (float)normalizedHorizontalDistance ) * (1.0 - normalizedVerticalDistance);
			//requiredRoll = 1f - normalizedVerticalDistance;

			// now apply noise to the probability.
			float noise = SimplexNoise.Noise.Generate (position.vector3, noiseFrequency1, noiseOctaves) * RegularnessControl;
			noise += SimplexNoise.Noise.Generate (position.vector3, noiseFrequency2, noiseOctaves) * ( 1f - RegularnessControl);
			noise = noise * 0.5f + 0.5f;

			Sector sector = new Sector ();
			sector.position = position;
			sector.probability = (float)probability * noise;

			return sector;
		}
	}

}