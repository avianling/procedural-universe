﻿using UnityEngine;
using System.Collections;

namespace Alex.Procedural.Universe {
	
	/// <summary>
	/// Star fabricator.
	/// Defines the functions used to create a star in the needed contexts.
	/// </summary>
	public class FabricationManager : MonoBehaviour {
		
		public StarFabricator starFabricator;
		
	}

}