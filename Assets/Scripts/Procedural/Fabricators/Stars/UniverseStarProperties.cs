﻿using UnityEngine;
using System.Collections;

public class UniverseStarProperties : Singleton<UniverseStarProperties> {

	[Header("The range(in au) at which stars will not appear to shrink any further")]
	public float appearAtRange = 1f;

}
