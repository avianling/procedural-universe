﻿using UnityEngine;
using System.Collections;

namespace Alex.Procedural.Universe {

	public class UniverseStar_1 : Poolable<UniverseStar_1, Star, ProceduralView >, UniverseStar_Base {

		public SpriteRenderer[] renderer;
		public SpriteRenderer aura;
		private float initialAuraAlpha;

		// This star is scaled to be at a constant size to simulate the way that stars are still really bright even at great distances
		// like whoa

		void Awake() {
			initialAuraAlpha = aura.color.a;
		}

		// The highest detail version of the star.
		// Faces the player.
		protected override void init (Star state, ProceduralView view)
		{
			// Configure this star with the properties 
			view.fabricator.starFabricator.configureStar_Universe (this, state);

			/*for (int i=0; i < renderer.Length; i++) {
				float a = renderer[i].color.a;
				Color temp = state.color;
				temp.a = a;
				renderer[i].color = temp;
			}*/

			transform.localScale = transform.localScale * ScaleTool.ScaleObject (gameObject, param2.gamePositionCenter, param2.model.universe.SectorSizeInAU * param2.gameSizeOfAU * UniverseStarProperties.getSingleton ().appearAtRange);
			if (state.size > 1f) {
				Color temp = aura.color;
				temp.a = initialAuraAlpha * state.size * state.size;
				aura.color = temp;
			}
		}

		public void PositionStar() {
			param2.fabricator.starFabricator.positionStar_Universe (gameObject, param1);
			transform.LookAt (Camera.main.transform.position);
		}

		public Star getStar() {
			return param1;
		}

		public GameObject getGameObject() {
			if (this != null) {
				return gameObject;
			} else {
				return null;
			}
		}

		void OnEnable() {
			if (param1 != null) {
				StartCoroutine(fadeIn());
			}
		}
		
		IEnumerator fadeIn() {
			yield return 0;
			float _time = 0f;

			Color[] start = new Color[renderer.Length];
			Color[] end = new Color[renderer.Length];
			float a;
			for (int i=0; i < renderer.Length; i++) {
				a = renderer[i].color.a;
				end[i] = param1.color;
				end[i].a = a;
				start[i] = param1.color;
				start[i].a = 0;
			}

			
			while (_time < 1f) {
				_time += Time.deltaTime;
				for ( int i=0; i < renderer.Length; i++ ) {
					renderer[i].color = Color.Lerp( start[i], end[i], _time );
				}
				yield return 0;
			}
		}
	}

}