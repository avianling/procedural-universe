﻿using UnityEngine;
using System.Collections;

namespace Alex.Procedural.Universe {
	
	public class SimpleStarFabricator : StarFabricator {

		public GameObject[] starPrefabs;
	
		public override GameObject fabricateStar_Universe (Star star, int lod = 0)
		{
			try {
				GameObject obj = Instantiate<GameObject>(starPrefabs[lod]);
				//obj.transform.localScale = Vector3.one * star.age;
				return obj;
			} catch ( System.IndexOutOfRangeException e ) {
				Debug.LogException(e);
				Debug.LogError("error: Tried to access a lod which didn't exist ( probably ) ", this );
				return Instantiate<GameObject>(starPrefabs[0]);
			}
		}

		public override void configureStar_Universe (UniverseStar_Base newStar, Star star)
		{
			GameObject starObj = newStar.getGameObject ();
			/*star = model.fabricator.starFabricator.fabricateStar_Universe( stars[i] );
			star.transform.localScale = Vector3.one * ( ((stars[i].size * model.universe.StarSizeInAU) / model.universe.SectorSizeInAU) * gameSizeOfSector);
			star.transform.parent = transform;
			star.transform.LookAt(Camera.main.transform);*/

			positionStar_Universe (starObj, star);
			starObj.transform.localScale = Vector3.one * (star.size * view.model.universe.StarSizeInAU * view.gameSizeOfAU);
			starObj.transform.parent = view.transform;
			starObj.transform.LookAt (Camera.main.transform);
		}

		public override void positionStar_Universe (GameObject obj, Star star)
		{
			obj.transform.position = view.gameSizeOfSector * (star.sector.position.vector3 - view.area.center) + (0.5f * star.position * view.gameSizeOfSector);
		}


	}
}