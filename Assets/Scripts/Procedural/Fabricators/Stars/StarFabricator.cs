﻿using UnityEngine;
using System.Collections;

namespace Alex.Procedural.Universe {
	
	public abstract class StarFabricator : MonoBehaviour {

		public ProceduralView view;
		
		public abstract GameObject fabricateStar_Universe( Star star, int lod = 0 );

		public abstract void configureStar_Universe( UniverseStar_Base newStar, Star star );

		public abstract void positionStar_Universe( GameObject obj, Star star );
	
	}

}