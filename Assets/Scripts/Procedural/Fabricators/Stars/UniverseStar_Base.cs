﻿using UnityEngine;
using System.Collections;

namespace Alex.Procedural.Universe {

	/// <summary>
	/// A common root class of the different star representations present in the universe.
	/// Provides the current star fabricator with the tools to configure this star.
	/// </summary>
	public interface UniverseStar_Base {

		//void init( Star star, ProceduralModel model );

		GameObject getGameObject();

		void PositionStar();

		Star getStar();

		void release();

	}

}