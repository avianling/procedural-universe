﻿using UnityEngine;
using System.Collections;

namespace Alex.Procedural.Universe {

	public class UniverseStar_0 : Poolable<UniverseStar_0, Star, ProceduralView >, UniverseStar_Base {

		public SpriteRenderer renderer;
		public SpriteRenderer aura;
		private float initialAuraAlpha;
		public GameObject brightCenter;

		private Vector3 origScale;

		private StarIcon _hudIcon;

		void Awake() {
			initialAuraAlpha = aura.color.a;
		}

		// The highest detail version of the star.
		// Faces the player.
		protected override void init (Star state, ProceduralView view)
		{
			// Configure this star with the properties 
			view.fabricator.starFabricator.configureStar_Universe (this, state);
			origScale = transform.localScale;

			float a = renderer.color.a;
			Color temp = state.color;
			temp.a = a;
			renderer.color = temp;

			// Tint the aura
			if (state.size > 1f) {
				temp = aura.color;
				temp.a = initialAuraAlpha * state.size * state.size;
				aura.color = temp;
			}

			rescaleStar ();
		}

		private void rescaleStar() {
			transform.localScale = origScale * ScaleTool.ScaleObject (gameObject, param2.gamePositionCenter, param2.model.universe.SectorSizeInAU * param2.gameSizeOfAU * UniverseStarProperties.getSingleton ().appearAtRange);
		}

		public void PositionStar() {
			param2.fabricator.starFabricator.positionStar_Universe (gameObject, param1);
			transform.LookAt (Camera.main.transform);
		}

		public Star getStar() {
			return param1;
		}

		void OnEnable() {
			StartCoroutine (fadeIn ());
		}

		IEnumerator fadeIn() {
			yield return 0;
			float _time = 0f;

			float a = renderer.color.a;
			Color end = param1.color;
			end.a = a;
			Color start = param1.color;
			start.a = 0;

			while (_time < 1f) {
				_time += Time.deltaTime;
				renderer.color = Color.Lerp( start, end, _time );
				yield return 0;
			}
		}

		public GameObject getGameObject() {
			if (this != null) {
				return gameObject;
			} else {
				return null;
			}
		}

		void Update() {
			rescaleStar();

			float cameraDotFactor = Vector3.Dot (Camera.main.transform.forward, (transform.position - Camera.main.transform.position).normalized);
		
			if (_hudIcon == null) {
				if (cameraDotFactor > 0.97f) {
					_hudIcon = HUDFactory.CreateStarIcon (this);
				}
			} else {
				if ( cameraDotFactor < 0.5f ) {
					Destroy(_hudIcon.gameObject);
					_hudIcon = null;
				}
			}
		}

		protected override void onRelease ()
		{
			base.onRelease ();
			if (_hudIcon != null) {
				Destroy (_hudIcon.gameObject);
				_hudIcon = null;
			}
		}
	}

}