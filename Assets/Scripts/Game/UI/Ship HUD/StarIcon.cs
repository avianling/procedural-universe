﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Star icon.
/// An icon for a LOD0 star.
/// </summary>
public class StarIcon : HUDElement {

	private Image _image;

	public void init( GameObject star ) {
		CameraSpaceBillboard billboard = gameObject.AddComponent<CameraSpaceBillboard> ();
		billboard.ourCamera = ShipHUD.getSingleton ().uiCamera;
		billboard.target = star;
		billboard.targetCamera = ShipHUD.getSingleton ().mainGameCamera;
		_image = GetComponent<Image> ();
	}

	public float intensity {
		set {
			_image.color = new Color(1f,1f,1f,value );
		}
	}

}
