﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShipHUD : Singleton<ShipHUD> {

	public Camera uiCamera;
	public Camera mainGameCamera;
	protected List<HUDElement> hudElements = new List<HUDElement>();

	public void AddHUDElement( HUDElement element ) {
		element.transform.parent = transform;
		hudElements.Add (element);
	}
}
