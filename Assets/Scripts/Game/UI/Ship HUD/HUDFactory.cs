﻿using UnityEngine;
using System.Collections;
using Alex.Procedural.Universe;

/// <summary>
/// HUD factory.
/// A factory for HUD elements.
/// </summary>
public static class HUDFactory {

	public static StarIcon CreateStarIcon( UniverseStar_0 star ) {
		StarIcon icon = GameObject.Instantiate<StarIcon>(Library.getSingleton().starIcon);
		icon.init (star.gameObject);
		ShipHUD.getSingleton ().AddHUDElement (icon);
		return icon;
	}

}
