﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScreenShake : Singleton<ScreenShake> {

	private class Shake {
		public float duration;
		public float intensity;
	}

	private List<Shake> _shakes = new List<Shake>();

	public Camera camera;

	public void ShakeCamera( float intensity, float duration ) {
		Shake s = new ScreenShake.Shake ();
		s.duration = duration;
		s.intensity = intensity;
		_shakes.Add (s);
	}
	
	// Update is called once per frame
	void Update () {
		float shakeMagnitude = 0;
		for ( int i=0; i < _shakes.Count; i++ ) {
			_shakes[i].duration -= Time.deltaTime;
			if ( _shakes[i].duration <= 0f ) {
				_shakes.RemoveAt(i);
				i--;
				continue;
			} else {
				shakeMagnitude += _shakes[i].intensity;
			}
		}

		camera.transform.localEulerAngles = new Vector3( Random.Range (-0.5f, 0.5f) * shakeMagnitude, Random.Range (-0.5f, 0.5f) * shakeMagnitude, 0f );
	}
}
