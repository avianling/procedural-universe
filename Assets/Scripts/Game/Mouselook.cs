﻿using UnityEngine;
using System.Collections.Generic;

public class Mouselook : MonoBehaviour {

	public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
	public RotationAxes axes = RotationAxes.MouseXAndY;
	public float sensitivityX = 15F;
	public float sensitivityY = 15F;

	public float minimumX = -360F;
	public float maximumX = 360F;

	public float minimumY = -60F;
	public float maximumY = 60F;

	float rotationX = 0F;
	float rotationY = 0F;

	Quaternion originalRotation;

	void Update ()
	{
		if (axes == RotationAxes.MouseXAndY)
		{
			// Read the mouse input axis
			rotationX += Input.GetAxis("Mouse X") * sensitivityX;
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
			
			rotationX = ClampAngle (rotationX, minimumX, maximumX);
			rotationY = ClampAngle (rotationY, minimumY, maximumY);
			
			Quaternion xQuaternion = Quaternion.AngleAxis (rotationX, Vector3.up);
			Quaternion yQuaternion = Quaternion.AngleAxis (rotationY, -Vector3.right);
			
			transform.localRotation = originalRotation * xQuaternion * yQuaternion;
		}
		else if (axes == RotationAxes.MouseX)
		{
			rotationX += Input.GetAxis("Mouse X") * sensitivityX;
			rotationX = ClampAngle (rotationX, minimumX, maximumX);
			
			Quaternion xQuaternion = Quaternion.AngleAxis (rotationX, Vector3.up);
			transform.localRotation = originalRotation * xQuaternion;
		}
		else
		{
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
			rotationY = ClampAngle (rotationY, minimumY, maximumY);
			
			Quaternion yQuaternion = Quaternion.AngleAxis (-rotationY, Vector3.right);
			transform.localRotation = originalRotation * yQuaternion;
		}

		if (Input.GetKey (KeyCode.UpArrow)) {
			transform.Translate( Vector3.forward * speed * Time.deltaTime );
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			transform.Translate( -Vector3.forward * speed * Time.deltaTime );
		}
		if (Input.GetKey (KeyCode.LeftArrow)) {
			transform.Translate( Vector3.left * speed * Time.deltaTime );
		}
		if (Input.GetKey (KeyCode.RightArrow)) {
			transform.Translate( Vector3.right * speed * Time.deltaTime );
		}

		if (Input.GetKey (KeyCode.UpArrow)) {
			ScreenShake.getSingleton().ShakeCamera( 0.8f, Time.deltaTime );
			_time = Mathf.Min( 1f, _time + Time.deltaTime );
		} else {
			_time = Mathf.Max( 0f, _time - Time.deltaTime * 5f);
		}

		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			ScreenShake.getSingleton().ShakeCamera( 0.3f, 1f );
		}
		if (Input.GetKeyUp (KeyCode.UpArrow)) {
			ScreenShake.getSingleton().ShakeCamera( 1.2f, 0.5f );
		}

		camera.fieldOfView = Mathf.Lerp (startingFOV, endingFOV, _time);
	}

	public float speed = 90f;
	public Camera camera;
	public float startingFOV = 60f;
	public float endingFOV = 140f;
	private float _time;

	void Start ()
	{
		Cursor.visible = false;

		Rigidbody rigidbody = GetComponent<Rigidbody> ();
		// Make the rigid body not change rotation
		if (rigidbody)
			rigidbody.freezeRotation = true;
		originalRotation = transform.localRotation;
	}

	public static float ClampAngle (float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp (angle, min, max);
	}
}