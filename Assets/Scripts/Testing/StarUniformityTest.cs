﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Alex.Procedural.Universe;


public class StarUniformityTest : MonoBehaviour {

	public ProceduralModel model;

	public int3 position;

	public List<Star> sampleStars = new List<Star> ();

	// Use this for initialization
	void Generate () {
		sampleStars.Clear ();

		Sector sector = model.generator.universeGenerator.generateSector (position, model.universe);

		for (int i=0; i < 20; i++) {
			Star[] stars = model.generator.starGenerator.GenerateStarsForSector( sector, model.universe );
			sampleStars.AddRange( stars );
		}

		Debug.Log ("Done!");
	}

	// Test for collisions in a hashset.
	IEnumerator TestHashSet() {
		HashSet<Star> starSet = new HashSet<Star> ();

		bool foundAnything = false;

		// There should be no collisions here.
		for (int x=0; x < 50; x++) {
			for (int y=0; y < 50; y++) {
				for (int z=0; z < 50; z++) {
					Sector sector = model.generator.universeGenerator.generateSector (new int3(x,y,z), model.universe);
					
					Star[] stars = model.generator.starGenerator.GenerateStarsForSector( sector, model.universe );

					if ( stars.Length > 0 ) {
						foundAnything = true;
						if ( starSet.Contains( stars[0] ) ) {
							Debug.LogError ("Got a collision where there shouldn't have been any :(");
						} else {
							starSet.Add( stars[0] );
						}
					}
				}

			}
			//Debug.Log ((float)x/50f);
			yield return 0;
		}

		if (!foundAnything) {
			Debug.LogError ("Didn't find any stars");
		} else {
			Debug.Log ("Found " + starSet.Count + " stars");
		}

		// Now test for collisions!
		// Should be a collision.

		// There should be no collisions here.
		for (int x=0; x < 50; x++) {
			for (int y=0; y < 50; y++) {
				for (int z=0; z < 50; z++) {
					Sector sector = model.generator.universeGenerator.generateSector (new int3(x,y,z), model.universe);
					
					Star[] stars = model.generator.starGenerator.GenerateStarsForSector( sector, model.universe );
					
					if ( stars.Length > 0 ) {
						foundAnything = true;
						if ( !starSet.Contains( stars[0] ) ) {
							Debug.LogError ("Didn't find a collision where there should have been one :(");
						}
					}
				}
			}
			yield return 0;
			//Debug.Log ((float)x/50f);
		}

	}

	void Update() {
		if ( Input.GetKeyDown(KeyCode.Backspace) ) {
			Generate();
		}

		if ( Input.GetKeyDown (KeyCode.T) ) {
			StartCoroutine(TestHashSet());
		}
	}
	

}
