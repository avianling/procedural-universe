﻿using UnityEngine;
using System.Collections;

public class DistanceScalingTest : MonoBehaviour {

	private Vector3 initialScale;
	public float maxDistance = 10f;

	// Use this for initialization
	void Start () {
		initialScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		transform.localScale = initialScale * ScaleTool.ScaleObject (gameObject, Vector3.zero, maxDistance);
	}
}
