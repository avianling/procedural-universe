﻿using UnityEngine;
using System.Collections;

public class CameraSpaceBillboard : MonoBehaviour {

	[Header("The object to follow")]
	public GameObject target;
	[Header("The camera the target will appear in")]
	public Camera targetCamera;
	[Header("The camera which we should appear for")]
	public Camera ourCamera;

	
	// Update is called once per frame
	void Update () {
		if (target == null) {
			Destroy (gameObject);
		} else {
			Vector3 viewportPos = targetCamera.WorldToViewportPoint (target.transform.position);
			transform.position = ourCamera.ViewportToWorldPoint (viewportPos);
		}
	}
}
