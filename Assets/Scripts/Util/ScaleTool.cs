﻿using UnityEngine;
using System.Collections;

public class ScaleTool {

	/// <summary>
	/// Scale the object such that it appears no smaller than it would at the specified distance.
	/// </summary>
	/// <param name="obj">Object.</param>
	/// <param name="distance">Distance.</param>
	/// <param name="size">Size.</param>
	public static float ScaleObject( GameObject obj, Vector3 origin, float distance ) {
		float distanceFromCenter = (obj.transform.position - origin).magnitude;
		float smallestDistance = distance;
		if (distanceFromCenter > smallestDistance) {
			float angle = Mathf.Atan2 (1f, smallestDistance);
			float scaleModifier = (float)(distanceFromCenter * Mathf.Tan (angle));
			return scaleModifier;
		} else {
			return 1f;
		}
	}

}
