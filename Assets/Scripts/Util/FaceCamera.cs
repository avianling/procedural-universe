﻿using UnityEngine;
using System.Collections;

public class FaceCamera : MonoBehaviour {

	public enum Modes
	{
		Always,
		AtStartOnly
	}

	public Modes mode;

	// Use this for initialization
	void Start () {
		transform.LookAt (Camera.main.transform);
		if (mode != Modes.Always) {
			this.enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (Camera.main.transform);
	}
}
