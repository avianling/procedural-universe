﻿Shader "Custom/CrossSectionShader" {
	Properties {
		_Volume ("Volume", 3D) = "" {}
		_Section ("Section", Range(0,1) ) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		Pass {
			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma vertex vert
			#pragma fragment frag

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0
			
			#include "UnityCG.cginc"

			sampler3D _Volume;
			uniform float _Section;
			
			struct v_in {
				float4 v : POSITION;
			};
			
			struct p_in {
				float4 pos : POSITION;
				float3 texPos : TEXCOORD0;
			};
			
			p_in vert( v_in i ) {
				p_in o;
				i.v.x = _Section - 0.5;
				o.pos = mul(UNITY_MATRIX_MVP, i.v );
				o.texPos = i.v.xyz * 0.5 + 0.5;
				//o.texPos.y = _Section;
				return o;
			}
			
			float4 frag( p_in i ) : COLOR {
				return tex3D( _Volume, i.texPos );
				//return float4( 1,0,0,1);
			}
			
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
