﻿Shader "Custom/SunShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Normal", 2D) = "white" {}
		_RimPower ("Rim Power", float ) = 3
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float3 viewDir;
			float3 worldNormal;
		};

		fixed4 _Color;
		uniform float _RimPower;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = (tex2D (_MainTex, IN.uv_MainTex + fixed2( _Time.x, _Time.x*.75) ) * tex2D (_MainTex, 0.4*IN.uv_MainTex + 0.4*fixed2( _Time.x, -_Time.x) ) + tex2D(_MainTex, IN.uv_MainTex * 0.1 + 0.04*fixed2(-_Time.x, _Time.x))) * 0.5 * _Color;
			half rim = pow(saturate(dot (normalize(IN.viewDir), o.Normal)), _RimPower);
			//o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Emission = _Color * (1-rim) + c * rim;
			
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
